# Flash Report: {Topic Name}

## Key Takeaways

- ...
- ...

## Threat Overview

### Executive Summary

...

### Technical Details

## Threat Actor Profile

- ...
- ...

## Indicators of Compromise (IoCs)

- ...
- ...

## Threat Assessment

**Likelihood of Attack**: (Low/Medium/High)

- Reasoning: (Brief explanation)
**Confidence Level**: (Low/Medium/High)

- Reasoning: (brief explanation)

**Potential Impact**:

1. (Impact 1): (Severity - Low/Medium/High)
   - (Brief explanation)
2. (Impact 2, if applicable): (Severity - Low/Medium/High)
   - (Brief explanation)

**Current Capabilities**:

- Detection Capabilities: (Poor/Fair/Good)
  - (Brief explanation)
- Prevention Capabilities: (Poor/Fair/Good)
  - (Brief explanation)

## Recommendations

- ...
- ...

## References

- ...
- ...

/label ~"TIReport::Flash Report"

# Threat Briefing: {Month / Year}

## Key Takeaways

- ...
- ...

## Threat Landscape Overview

> _(Summary of the current threat environment, highlighting the most significant changes or trends)_

## Highlighted Threats

### Threat 1: {Threat Name}

> _(Short overview of the threat, highlighting its relevance to GitLab)_

- Current capabilities:
  - Detection: (Poor/Fair/Good)
    - (Brief explanation)
  - Prevention: (Poor/Fair/Good)
    - (Brief explanation)

- Recommendations:
  - ...
  - ...

### Threat 2: {Threat Name}

> _(Short overview of the threat, highlighting its relevance to GitLab)_

- Current capabilities:
  - Detection: (Poor/Fair/Good)
    - (Brief explanation)
  - Prevention: (Poor/Fair/Good)
    - (Brief explanation)

- Recommendations:
  - ...
  - ...

## References

- ...
- ...

/label ~"TIReport::Threat Briefing"
